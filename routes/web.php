<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StateController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('state')->group(function () {
    Route::get('all', [StateController::class, 'all']);

    Route::get('create', [StateController::class, 'create']);

    // TODO. Create edit form.
    // Use POST instead of GET to submit.
    // Create admin form.
    
    Route::get('{id}', [StateController::class, 'stateById']);
    Route::get('abbr/{abbr}', [StateController::class, 'stateByAbbr']);
});
