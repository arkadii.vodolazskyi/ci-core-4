<x-header>
  Data

  <x-slot:subtitle>
    Subtitle
  </x-slot>
</x-header>

<ul>
  @foreach ($states as $state)
    <li>
      {{ $state->name }}, 
      {{ $state->abbr }}
    </li>
  @endforeach
</ul>