<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStateRequest;
use Illuminate\Http\Request;
use App\Models\State;

class StateController extends Controller
{
    public function all()
    {
        $states = State::all();
        return view('states', [
            'states' => $states,
        ]);
    }

    public function stateById(int $id) 
    {
        $state = State::find($id);
        dd($state);
    }

    public function stateByAbbr(string $abbr) 
    {
        $state = State::where('abbr', $abbr)->first();
        dd($state);
    }

    public function create(CreateStateRequest $request) 
    {
        $data = $request->validated();
        
        $state = State::create($data);
        // $state = new State();
        // $state->name = $data['name'];
        // $state->abbr = $data['abbr'];
        // $state->save();

        return $state;
    }
}
